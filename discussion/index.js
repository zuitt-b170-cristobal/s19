//JS ES6 Updates
	//ECMAScript - tech used to create the languages such as JavaScript
//exponent operator
	//pre-es6
	const firstNum = Math.pow(8, 2);
	console.log(firstNum);
	//es6
	const secondNum = 8**2;
	console.log(secondNum);

//TEMPLATE LITERALS
	//allows writing strings w/o use of concat operator (+); helps in terms of readability of codes & efficiency of work

	//MiniActivity
//pre es6
let name = "Zip";
let message = "Hello " + name + "! Welcome to the programming field."

console.log(message);


//es6
message=`Hello ${name}! Welcome to the programming field.`;
console.log(message);

//Multiline
const anotherMessage = `
${name} attended a math competition.
He won it by solving the problem 8**2 with the answer of ${secondNum}
`
console.log(anotherMessage);

//computation inside template literals
const interestRate = .10;
const principal = 1000;
console.log(`The interest on your savings is ${principal * interestRate}`);

/*
Array Destructuring - allows unpacking elements in array into distinct variables; allows naming of elements instead of index numbers. 
SYNTAX:
	let/const [variableA, variableB, variableC,...variableN] = arrayName
*/

//MiniActivity
//pre-es6
let fullName = ["firstName", "middleName", "lastName"]
console.log(fullName[0]);
console.log(fullName[1]);
console.log(fullName[2]);
console.log(`Hello ${fullName[0]}`);//COPY CODE


//es6
const [firstName, middleName, lastName] = fullName;
console.log(firstName);
console.log(middleName);
console.log(lastName);
//COPY CODE


//Object Destructuring - allows unpacking elements in object into distinct COPY CODE
let woman = {
	givenName: "Anna", 
	maidenName: "Karen", 
	familyName: "Nina"
}
console.log(woman.givenName);
console.log(woman.maidenName);
console.log(woman.familyName);
console.log(`Hello ${woman.givenName} ${woman.maidenName} ${woman.familyName}!`);

//es6
const {givenName, maidenName, familyName} = woman;
console.log(givenName);
console.log(maidenName);
console.log(familyName);
console.log(`Hello ${givenName} ${maidenName} ${familyName}!`);

//MiniActivity
/*
Arrow Function
compact alternative syntax to traditional functions; useful for code snippets where creating functions will be reused in any other parts of code; Dont repeat yourself - no need to create function that will not be used in the other parts/portions of code 

*/
/*
parts of function

*/


 function fulname(firstName, midName, lastName) {
 //	console.log(firstName)
 //	console.log(midName)
 //	console.log(lastName)
 	console.log(firstName, midName, lastName)
}

fulname("Ana", "Karen", "Nina");

//es6
const printFName=(fname, mname, lname) =>{
	console.log(fname, mname, lname);
}
printFName("Will", "D.", "Smith");



//Miniactivity
/*
Arrow functions w loop/methids
uses forEach method to log each student in console
*/
//pre-es6
const students = ['John', 'Jane', 'Joe']
students.forEach(
	function(element){
		console.log(element);
	}
);

//es6
	//if you have 2 or more parameters, enclose them inside a pair of parenthesis
students.forEach(x => console.log(x));


//Arrow Functions - Implicit Return Statement 
	//return statement/s are omitted because JS implicitly add them for the resulty of the function
//MiniActivity
//pre-es6
function plus(a, b) {
	return a + b;
}
let total = plus (7, 10)
console.log(total);

//es6
const adds = (x, y) => x + y //no curly braces if only 1 statement; add curly braces if 2 or more statements
	//code above runs like: const adds = (x, y) => return x + y
;
let totals = adds(4,6);
console.log(totals);


//Arrow Function - Default Function Arguement Value
/*
provides a default argument value if no parameters are specified once function has been invoked.

*/
const greet = (name = "User") => {
	return `Good morning, ${name}`
}
console.log(greet());
//once function has specified parameter value
console.log(`Result of specified value for parameter: ${greet("Lisa")}`);

//MiniActivity
/*
function car(name, brand, year){
	this.name = name
	this.brand = brand
	this.year = year
};
const car1 = new car("Fortuner", "Toyota", 2010);
console.log(car1);
*/

//es6
//Class Constructor 
	//class keyword declares creation of "car" object
class car{
	//constructor keyword - special method of creating/initializing object for the "car" class
	constructor (brand, name, year){
		//this - sets properties that are included to the object "car" class
		this.brand = brand,
		this.name = name,
		this.year = year
	}
};
const car1 = new car("Ford", "Ranger Raptor", 2021);
console.log(car1);


const car2 = new car();
console.log(car2);
car2.brand = "Toyota",
car2.name = "Fortuner",
car2.name = 2020
console.log(car2);

//MiniAct

let num = 10;
(num <= 0) ? console.log(true) : console.log(false);

/* ANOTHER WAY TO ANSWER:
if (num <= 0){
	console.log(true)
}else{
	console.log(false)
}
*/

















































































