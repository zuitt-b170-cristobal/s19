
const getCube = 8**3;
	console.log(`The cube of 8 is ${getCube}`);

let address = ["258", "Washington Ave", "NW, California", "90011"]
 
let numb = 258;
let street = "Washington Ave";
let state = "NW, California";
let zipC = "90011";
//const [258, Washington Ave, NW, California, 90011] = address;
//console.log("I live at " + address[0] + " " + address[1] + " " + address[2]+ " " + address[3]);
console.log(`I live at ${numb} ${street} ${state} ${zipC}`);


let animal = {
	givenName: "Lolong", 
	animalType: "Saltwater crocodile", 
	animalWeight: "1075 kgs",
	measurement: "20 ft 3 in"
}

const {givenName, animalType, animalWeight, measurement} = animal;
console.log(`${givenName} ${animalType} ${animalWeight} ${measurement}`);

const numbers = [1,2,3,4,5];
numbers.forEach(x => console.log(x));

const adds = (a, b, c, d, e) => a + b + c + d + e;
let totals = adds(1, 2, 3, 4, 5);
console.log(totals);

class dog{
	// constructor keyword - special method of creating/initializing an object for the "car" class
	constructor (name, age, breed){
		// this - sets the properties that are included to the "car" class
		this.name = name,
		this.age = age,
		this.breed = breed
	}
};
const dog1 = new dog("Frankie", "5", "Miniachure Dachshund");
console.log(dog1);

